import java.util.*;

public class Game {

    /*

//                "\u001b[31;1mRed\u001b[0m",
//                "\u001b[32;1mGreen\u001b[0m",
//                "\u001b[33;1mYellow\u001b[0m",
//                "\u001b[34;1mBlue\u001b[0m",
//                "\u001b[35;1mPurple\u001b[0m",
//                "\u001b[36;1mCyan\u001b[0m"

     */

    private String[] answer;
    private Scanner in = new Scanner(System.in);

    public void start() {

        System.out.println("Welcome to Mastermind!");
        System.out.println("======================");

        int input;

        do {

            System.out.println();
            System.out.println("Please choose an option: ");
            System.out.println("0 - Exit");
            System.out.println("1 - New Game");
            System.out.print("> ");
            input = in.nextInt();
            in.nextLine();

            switch (input) {
                case 0:
                    System.out.println("Bye!");
                    System.exit(0);
                    break;
                case 1:
                    play();
                    break;
                default:
                    break;
            }

        } while (true);


    }

    private void play() {
        generateAnswer();
        System.out.println();
        System.out.println("Computer has made their guess!");

        String input;
        int[] score;

        do {

            System.out.print("> ");
            input = in.nextLine();
            score = calculateScore(input);

            if (Arrays.equals(score, new int[]{4, 0})) {
                // Won
                System.out.println("You have won!");
                break;
            }

            for (int i = 0 ; i < score[0] ; i++) {
                System.out.print("\u001b[31;1mRed \u001b[0m");
            }

            for (int i = 0 ; i < score[1] ; i++) {
                System.out.print("\u001b[33;1mYellow \u001b[0m");
            }

            System.out.println();

        } while (true);

    }

    private void generateAnswer() {

        ArrayList<String> colours = new ArrayList<>(
            Arrays.asList("Red", "Green", "Yellow", "Blue", "Purple", "Cyan")
        );

        this.answer = new String[4];

        for (int i = 0 ; i < answer.length ; i++) {
            Collections.shuffle(colours);
            answer[i] = colours.remove(0);
        }
    }

    private int[] calculateScore(String input) {
        String[] scores = input.split(" ");
        int red = 0;
        int yellow = 0;

        for (int i = 0 ; i < 4 ; i++) {
            if (scores[i].equals(answer[i])) {
                red += 1;
            }
        }

        for (String s : scores) {
            for (String t : answer) {
                if (s.equals(t)) {
                    yellow += 1;
                }
            }
        }
        yellow -= red;

        return new int[] {red, yellow};
    }

}
